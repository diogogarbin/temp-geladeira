#include <Ethernet.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
#include <SPI.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS     3 

//VARIAVEIS
float temperatura;
char sentenca[128];
char valortemp[10];

//CONFIGURAÇÃO DA PLACA
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };  //Mac Address
byte ip[] = {192, 168, 0, 240};                     //IP Arduino
byte subnet[] = {255, 255, 255, 0};                 //Mascara rede
byte gateway[] = {192, 168, 0, 1};                  //Gateway

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress sensor1;


IPAddress server_addr(192,168,0,24);  // IP of the MySQL *server* here
char user[] = "USER";              // MySQL user login 
char password[] = "PASSWORD";      // MySQL password login
       
char INSERIR_TEMP[] = "INSERT INTO new_db_arduino.new_tmp_geladeira (sensor, valor) VALUES (%d, %s)";
char BANCODEDADOS[] = "USE new_db_arduino";
EthernetClient client;
MySQL_Connection conn((Client *)&client);

void setup() 
{ 
   Serial.begin(9600);
   while (!Serial); 
   Ethernet.begin(mac, ip, subnet, gateway);
   Serial.println("Conectando...");
   if (conn.connect(server_addr, 3306, user, password)) 
   {
      delay(1000);
      MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
      cur_mem->execute(BANCODEDADOS);
      delete cur_mem;
   }
   else
   {
      Serial.println("A conexão falhou");
      conn.close();
   }
}
void loop() 
{
   delay(40000);
   sensors.requestTemperatures();
   float tempC = sensors.getTempCByIndex(0);
   Serial.println(tempC);

   Serial.println("Executando inserção no banco");
     
   dtostrf(tempC, 6, 2, valortemp);
   sprintf(sentenca, INSERIR_TEMP, 2, valortemp);
   
   MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
   cur_mem->execute(sentenca);
   delete cur_mem;
}